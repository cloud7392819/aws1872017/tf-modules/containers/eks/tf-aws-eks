# ws@2024 key_pair.tf

##################################################################
## Create a public and private key pair for login to the EC2 Instances
##################################################################

resource "aws_key_pair" "default" {

  count = local.enabled ? 1 : 0

  key_name = local.key_pair_name
  
  public_key = local.public_key

  tags = merge (
    var.module_tags,
    {
      Name = local.key_pair_name
    }
  )

}