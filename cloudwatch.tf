# ws@2023 cloudwatch.tf

##################################################################
## Create log group for our service
##################################################################

resource "aws_cloudwatch_log_group" "default" {

  count = (local.enabled && local.cloudwatch_log_group_enabled) ? 1 : 0

  name  = local.cloudwatch_log_group_name

  retention_in_days = var.retention_in_days

  tags = merge(
    local.tags,
    {
      Name = local.cloudwatch_log_group_name
    }
  )
}

