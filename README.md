![Terraform](https://lgallardo.com/images/terraform.jpg)

---
# tf-aws-eks  [Terraform Module for EKS]
---

## VPC
```hcl
module "eks" { }
```

## EKS

```hcl
module "eks" { 

  source = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/containers/eks/tf-aws-eks.git" 

  ##################################################################
  # General/Common parameters
  ##################################################################

  # Whether to create the resources ("false" prevents the module from creating any resources).
  module_enabled = false

  region = var.region
  
  # Emulation of `depends_on` behavior for the module
  module_depends_on = [

  ]

  # A mapping of tags to assign to the resource.
  module_tags = {
    EKS = "true"
  }

  ##################################################################
  # EKS parameters
  ##################################################################

  # (Required) The name of the EKS.
  eks_name = "${var.cut_name}-eks"
  eks_version = "1.28"
  eks_service_ipv4_cidr       =  "172.20.0.0/16"

  # VPC Config
  eks_subnet_ids              = module.vpc.public_subnets

  eks_endpoint_public_access       = true
  eks_endpoint_public_access_cidrs = ["0.0.0.0/0"] 

  eks_endpoint_private_access = false 
  
  #vpc_name               = module.vpc.name
  #vpc_cidr_block         = module.vpc.cidr_block

  # Enable EKS Cluster Control Plane Logging
  enabled_eks_log_types = [
    "api", 
    "audit", 
    "authenticator", 
    "controllerManager", 
    "scheduler"
  ]

  # AWS IAM OIDC Connect Provider
  eks_oidc_enabled = false
  eks_oidc_root_ca_thumbprint = "9e99a48a9960b19766bb7f3b02e22da2b0ab7280"
  
  owners        = ""
  project       = ""
  scenario      = ""
  environment   = ""

  ##################################################################
  # EKS NodeGroupPublic
  ##################################################################

  # Node Group Public SPOT
  eks_ng_public_spot_enabled = false
  key_pair_name = "ws"

  # Public key for SSH access to EC2 instances
  public_key = file("${var.public_key_default}")



  ##################################################################
  # CloudWatch [Create log group for our service]
  ##################################################################

  # Whether to create the resources (`false` prevents the module from creating any resources).
  cloudwatch_log_group_enabled = false

}


