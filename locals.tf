# ws@2024 locals.tf

##################################################################
# General/Common 
##################################################################

locals {

  # Create module
  enabled = var.module_enabled ? true : false
  create  = var.module_enabled ? true : false    

  region = var.region

  eks_name    = var.eks_name
  #eks_id     = var.eks_id
  eks_version = var.eks_version
  eks_service_ipv4_cidr   = var.eks_service_ipv4_cidr

  # VPC Config
  eks_subnet_ids          = var.eks_subnet_ids

  eks_endpoint_public_access       = var.eks_endpoint_public_access
  eks_endpoint_public_access_cidrs = var.eks_endpoint_public_access_cidrs
  
  eks_endpoint_private_access = var.eks_endpoint_private_access

  # Enable EKS Cluster Control Plane Logging
  enabled_eks_log_types   = var.enabled_eks_log_types

  # AWS IAM OIDC Connect Provider
  eks_oidc_enabled = var.eks_oidc_enabled ? true : false
  # Extract OIDC Provider from OIDC Provider ARN
  aws_iam_oidc_connect_provider_extract_from_arn = element(split("oidc-provider/", "${join("", aws_iam_openid_connect_provider.oidc_provider.*.arn)}"), 1)

  # KeyPair
  key_pair_name = "${local.eks_name}-KeyPair"
  public_key = var.public_key


  # Node Group Public SPOT
  eks_ng_public_spot_enabled = var.eks_ng_public_spot_enabled ? true : false
  ec2_ssh_key = var.key_pair_name


  cloudwatch_log_group_name = "/${local.eks_name}-log-group/"

  owners      = var.owners 
  environment = var.environment 
  project     = var.project 
  scenario    = var.scenario 

  #
  cloudwatch_log_group_enabled = var.cloudwatch_log_group_enabled ? true : false

  tags = merge (
    var.module_tags,
    {
      owners        = local.owners
      project       = local.project
      environment   = local.environment
      scenario      = local.scenario 
    }
  )
}  

