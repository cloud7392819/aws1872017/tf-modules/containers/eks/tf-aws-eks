# ws@2024 variables.tf

##################################################################
# General/Common Variables
##################################################################

variable "module_enabled" {
  description   = "Whether to create the resources."
  type          = bool
  # `false` prevents the module from creating any resources
  default       = false
}

variable "owners" {
  description   = "The name owners"
  default       = "Unknown"
}

variable "project" {
  description   = "The name project"
  default       = "Unknown"
}

variable "environment" {
  description = "The environment"
  default = "Unknown"
}

variable "scenario" {
  description = "The name scenario"
  type        = any
  # Non zero length string can be used to have current module wait for the specified resource.
  default     = "Unknown"
}

variable "module_depends_on" {
  description = "Emulation of `depends_on` behavior for the module."
  type        = any
  # Non zero length string can be used to have current module wait for the specified resource.
  default     = ""
}

variable "module_tags" {
  description = "Additional mapping of tags to assign to the all linked resources."
  type        = map(string)
  default     = {}
}

# AWS Region
variable "region" {
  description	= "AWS Region"
  default 		= null
}


##################################################################
# EKS Variables
##################################################################

# EKS Cluster Input Variables
variable "eks_name" {
  description = "Name of the EKS cluster. Also used as a prefix in names of related resources."
  type        = string
  default     = "eksdemo"
}

variable "eks_version" {
  description = "Kubernetes minor version to use for the EKS cluster (for example 1.21)"
  type = string
  default     = null
}

variable "eks_subnet_ids" {
  description = ""
  default     = null
}

variable "eks_endpoint_public_access" {
  description = "Indicates whether or not the Amazon EKS public API server endpoint is enabled. When it's set to `false` ensure to have a proper private access with `cluster_endpoint_private_access = true`."
  type        = bool
  default     = true
}

variable "eks_endpoint_private_access" {
  description = "Indicates whether or not the Amazon EKS private API server endpoint is enabled."
  type        = bool
  default     = false
}

variable "eks_endpoint_public_access_cidrs" {
  description = "List of CIDR blocks which can access the Amazon EKS public API server endpoint."
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "eks_service_ipv4_cidr" {
  description = "service ipv4 cidr for the kubernetes cluster"
  type        = string
  default     = null
}

variable "enabled_eks_log_types" {
  description = "List of CIDR blocks which can access the Amazon EKS public API server endpoint."
  type        = list(string)
  default     = [
    "api", 
    "audit", 
    "authenticator", 
    "controllerManager", 
    "scheduler"
  ]
}

##################################################################
# AWS IAM OIDC Connect Provider
##################################################################

variable "eks_oidc_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}

# Input Variables - AWS IAM OIDC Connect Provider
# EKS OIDC ROOT CA Thumbprint - valid until 2037
variable "eks_oidc_root_ca_thumbprint" {
  type        = string
  description = "Thumbprint of Root CA for EKS OIDC, Valid until 2037"
  default     = "9e99a48a9960b19766bb7f3b02e22da2b0ab7280"
}

##################################################################
# Node Group Public SPOT Variables
##################################################################

variable "eks_ng_public_spot_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}

variable "key_pair_name" {
  description = ""
  type        = string
  default     = null
}

variable "public_key" {
  description = "Public key for SSH access to EC2 instances"
  type        = string
  default     = null
}

##################################################################
# 
##################################################################





##################################################################
# CloudWatch Variables
##################################################################

variable "cloudwatch_log_group_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}

# CloudWatch log group name
variable "cloudwatch_log_group_name" {
  description = "The CloudWatch log group name"
  default     = null
}

variable "retention_in_days" {
  description = "Retention period for Cloudwatch logs"
  default     = 7
  type        = number
}

variable "cloudwatch_log_group_tags" {
  description = "A mapping of tags to assign to the all underlying resources."
  type        = map(string)
  default     = {}
}

