
# IAM Role for EKS Node Group 
resource "aws_iam_role" "eks_nodegroup_role" {

  count = local.enabled ? 1 : 0

  name = "${local.eks_name}-EKSNodeGroupRole"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "eks-AmazonEKSWorkerNodePolicy" {

  count = local.enabled ? 1 : 0

  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = join("", aws_iam_role.eks_nodegroup_role.*.name)
}

resource "aws_iam_role_policy_attachment" "eks-AmazonEKS_CNI_Policy" {

  count = local.enabled ? 1 : 0

  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = join("", aws_iam_role.eks_nodegroup_role.*.name)
}

resource "aws_iam_role_policy_attachment" "eks-AmazonEC2ContainerRegistryReadOnly" {

  count = local.enabled ? 1 : 0

  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = join("", aws_iam_role.eks_nodegroup_role.*.name)
}

