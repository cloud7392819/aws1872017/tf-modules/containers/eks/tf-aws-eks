# ws@2023 variables.tf

##################################################################
# Common/General settings
##################################################################

# AWS Region
variable "region" {
  description	= "AWS Region"
  default 		= null
}

# Application name
variable "name" {
	description = "Application name"
	default		= null
}

# Application cut name
variable "cut_name" {
	description = "Application cut name"
	default		= null
}

# public key
variable "public_key_default" {
  description = "Public key default"
  default     = null
}

# domain name default
variable "domain_name_default" {
  description = "Domain zone"
  default     = null
}

##################################################################
# Environment settings
##################################################################

# Environment name
variable "environment" {
	description = "Environment name"
	default		= "backend"
}

# Owner Email
variable "ownerEmail" {
	description = "Owner Email"
	default		= null
}

# Team
variable "team" {
	description = "Team"
	default		= null
}

# DeployBy
variable "deployedby" {
	description = "DeployedBy"
	default		= null
}

