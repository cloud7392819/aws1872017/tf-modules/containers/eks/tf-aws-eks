# ws@2024 vpc.tf

##################################################################
# Create VPC Name: "${var.cut_name}-vpc"
##################################################################

locals {
  vpc_name = "${var.cut_name}-vpc-eks"
}


# Create VPC Terraform Module
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  #source = "./modules/vpc"
  version = "5.5.1"  
  
  # VPC Basic Details
  name = local.vpc_name
  
  cidr = "10.1.0.0/16"
  
  azs  = data.aws_availability_zones.default.names
  
  # Public subnets
  public_subnets  = [
    "10.1.1.0/24", 
    "10.1.2.0/24", 
    #"10.1.3.0/24", 
    #"10.1.4.0/24",
    #"10.1.5.0/24"
  ]
  
  # Private subnets
  private_subnets = [
    "10.1.11.0/24",
    "10.1.12.0/24", 
    #"10.1.13.0/24", 
    #"10.1.14.0/24",
    #"10.1.15.0/24"
  ] 

  # Database Subnets
  database_subnets = [
    "10.1.21.0/24",
    "10.1.22.0/24", 
    #"10.1.23.0/24", 
    #"10.1.24.0/24",
    #"10.1.25.0/24"
  ]
  
  # VPC Create Database Subnet Group (True / False)
  create_database_subnet_group = true

  # VPC Create Database Subnet Route Table (True or False)
  create_database_subnet_route_table = true
  
  # create_database_internet_gateway_route = true
  # create_database_nat_gateway_route = true
  
  # NAT Gateways - Outbound Communication
  # Enable NAT Gateways for Private Subnets Outbound Communication
  # VPC Enable NAT Gateway (True or False) 
  enable_nat_gateway = false

  # Enable only single NAT Gateway in one Availability Zone to save costs during our demo
  # VPC Single NAT Gateway (True or False)
  single_nat_gateway = true

  # VPC DNS Parameters
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    VPCName = local.vpc_name
    EKS = "true"
  }

  vpc_tags = {
    VPCName = local.vpc_name
    EKS = "true"
  }

  # Additional Tags to Subnets
  public_subnet_tags = {
    Type = "Subnets Public"
    VPCName = local.vpc_name
        
  }
  private_subnet_tags = {
    Type = "Subnets Private"
    VPCName = local.vpc_name
  
  }

  database_subnet_tags = {
    Type = "Subtens Database"
    VPCName = local.vpc_name
  }

  # Instances launched into the Public subnet should be assigned a public IP address.
  map_public_ip_on_launch = true

}