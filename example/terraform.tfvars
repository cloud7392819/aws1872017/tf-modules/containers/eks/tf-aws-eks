# ws@2023 terraform.tf

##################################################################
# TFVARS 
##################################################################

# Region
region   = "us-east-1"

# Application
name     = "<name"
cut_name = "<name>"

# Environment [info]
environment = "prod"
ownerEmail  = "user@mail.com"
team        = "DevOps"
deployedby  = "Terraform"

# Domain
domain_name_default = "<domain.com>"

# Key
public_key_default = "./keys/<name>.pub"