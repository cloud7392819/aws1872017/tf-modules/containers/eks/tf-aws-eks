# ws@2022 data.tf

##################################################################
# Common/General settings
##################################################################

data "aws_organizations_organization" "default" {}
data "aws_region" "default" {}
data "aws_caller_identity" "default" {}
data "aws_availability_zones" "default" {}

# --- Route53 ---
# data "aws_route53_zone" "default" {
#   name    = "${var.domain_name_default}"
# }