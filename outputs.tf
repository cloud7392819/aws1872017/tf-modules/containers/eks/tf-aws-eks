# ws@2024 outputs.tf

##################################################################
# EKS Outputs
##################################################################

# EKS Cluster Outputs
output "eks_id" {
  description = "The name/id of the EKS cluster."
  value       = concat(aws_eks_cluster.default.*.id, [""])[0]
}

output "eks_arn" {
  description = "The Amazon Resource Name (ARN) of the cluster."
  value       = concat(aws_eks_cluster.default.*.arn, [""])[0]
}
/*
output "eks_certificate_authority_data" {
  description = "Nested attribute containing certificate-authority-data for your cluster. This is the base64 encoded certificate data required to communicate with your cluster."
  value       = aws_eks_cluster.default[0].certificate_authority[0].data
}
*/
output "eks_endpoint" {
  description = "The endpoint for your EKS Kubernetes API."
  value       = concat(aws_eks_cluster.default.*.endpoint, [""])[0]
}

output "eks_version" {
  description = "The Kubernetes server version for the EKS cluster."
  value       = concat(aws_eks_cluster.default.*.version, [""])[0]
}

output "eks_iam_role_name" {
  description = "IAM role name of the EKS cluster."
  value       = join("", aws_iam_role.eks_master_role.*.name)
}

output "eks_iam_role_arn" {
  description = "IAM role ARN of the EKS cluster."
  value       = join("", aws_iam_role.eks_master_role.*.arn)
}
/*
output "eks_oidc_issuer_url" {
  description = "The URL on the EKS cluster OIDC Issuer"
  value       = aws_eks_cluster.default[0].identity[0].oidc[0].issuer
}

output "eks_primary_security_group_id" {
  description = "The cluster primary security group ID created by the EKS cluster on 1.14 or later. Referred to as 'Cluster security group' in the EKS console."
  value       = aws_eks_cluster.default[0].vpc_config[0].cluster_security_group_id
}
*/
##################################################################
# AWS IAM OIDC Connect Provider
##################################################################

# Output: AWS IAM Open ID Connect Provider ARN
output "aws_iam_openid_connect_provider_arn" {
  description = "AWS IAM Open ID Connect Provider ARN"
  value = join("", aws_iam_openid_connect_provider.oidc_provider.*.arn)
}

# Output: AWS IAM Open ID Connect Provider
output "aws_iam_openid_connect_provider_extract_from_arn" {
  description = "AWS IAM Open ID Connect Provider extract from ARN"
   value = local.aws_iam_oidc_connect_provider_extract_from_arn
}