
# Create IAM Role
resource "aws_iam_role" "eks_master_role" {

  count = local.enabled ? 1 : 0

  name = "${local.eks_name}-EKSMasterRole"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

# Associate IAM Policy to IAM Role
resource "aws_iam_role_policy_attachment" "eks-AmazonEKSClusterPolicy" {

  count = local.enabled ? 1 : 0

  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = join("", aws_iam_role.eks_master_role.*.name)
}

resource "aws_iam_role_policy_attachment" "eks-AmazonEKSVPCResourceController" {

  count = local.enabled ? 1 : 0

  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = join("", aws_iam_role.eks_master_role.*.name)
}

/*
# Optionally, enable Security Groups for Pods
# Reference: https://docs.aws.amazon.com/eks/latest/userguide/security-groups-for-pods.html
resource "aws_iam_role_policy_attachment" "eks-AmazonEKSVPCResourceController" {

  count = local.enabled ? 1 : 0

  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = join("", aws_iam_role.eks_master_role.*.name)
}
*/