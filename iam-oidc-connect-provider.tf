
# Datasource: AWS Partition
# Use this data source to lookup information about the current AWS partition in which Terraform is working
data "aws_partition" "default" {}

# Resource: AWS IAM Open ID Connect Provider
resource "aws_iam_openid_connect_provider" "oidc_provider" {

  count = (local.enabled && local.eks_oidc_enabled) ? 1 : 0

  client_id_list  = ["sts.${data.aws_partition.default.dns_suffix}"]
  thumbprint_list = [var.eks_oidc_root_ca_thumbprint]
  url             = aws_eks_cluster.default[0].identity[0].oidc[0].issuer

  tags = merge(
    {
      Name = "${var.eks_name}-eks-irsa"
    },
    local.tags
  )
}

