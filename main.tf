#

# Create AWS EKS Cluster
resource "aws_eks_cluster" "default" {
  
  count = local.enabled ? 1 : 0

  name     = "${local.eks_name}"

  role_arn = join("", aws_iam_role.eks_master_role.*.arn)

  version = local.eks_version

  vpc_config {
    subnet_ids              = local.eks_subnet_ids
    endpoint_public_access  = local.eks_endpoint_public_access
    public_access_cidrs     = local.eks_endpoint_public_access_cidrs
    endpoint_private_access = local.eks_endpoint_private_access    
  }

  kubernetes_network_config {
    service_ipv4_cidr      = local.eks_service_ipv4_cidr
  }
  
  # Enable EKS Cluster Control Plane Logging
  enabled_cluster_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]

  # Ensure that IAM Role permissions are created before and deleted after EKS Cluster handling.
  # Otherwise, EKS will not be able to properly delete EKS managed EC2 infrastructure such as Security Groups.
  depends_on = [
    aws_iam_role_policy_attachment.eks-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.eks-AmazonEKSVPCResourceController,
  ]
}